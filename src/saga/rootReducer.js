import { combineReducers } from "redux";
import GetInvestmentReducer from "./reducers/GetInvestmentReducer";
import AddInvestmentReducer from "./reducers/AddInvestmentReducer";
import EditInvestmentReducer from "./reducers/EditInvestmentReducer";
import DeleteInvestmentReducer from "./reducers/DeleteInvestmentReducer";
export default combineReducers({
  GetInvestmentReducer,
  AddInvestmentReducer,
  EditInvestmentReducer,
  DeleteInvestmentReducer
});

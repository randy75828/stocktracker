import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./rootReducer";
import createSagaMiddleware from "@redux-saga/core";
import { rootSagas } from "./rootSagas";

const sageMiddleWare = createSagaMiddleware();



const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sageMiddleWare))
);
sageMiddleWare.run(rootSagas);


export default store;

import axios from "axios";

const rootUrl = process.env.REACT_APP_API_ENDPOINT || "http://localhost:5000/";

const API = axios.create({
  baseURL: `${rootUrl}`,
  headers: { "Cache-Control": "no-cache" },
});

export default API;

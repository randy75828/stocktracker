import {
  ADD_NEW_INVESTMENT_PORTFOLIO,
  ADD_NEW_INVESTMENT_PORTFOLIO_OK,
  RESET_ADD_NEW_INVESTMENT_PORTFOLIO,
  GET_INVESTMENT_PORTFOLIOS,
  GET_INVESTMENT_PORTFOLIOS_OK,
  EDIT_INVESTMENT_PORTFOLIO,
  EDIT_INVESTMENT_PORTFOLIO_OK,
  RESET_EDIT_INVESTMENT_PORTFOLIO,
  DELETE_INVESTMENT_PORTFOLIO,
  DELETE_INVESTMENT_PORTFOLIO_OK,
  GET_INVESTMENT_PORTFOLIOS_AFTER_DELETION
} from "./InvestmentActionTypes";

export const getInvestmentPortfolios = () => {
  return {
    type: GET_INVESTMENT_PORTFOLIOS,
  };
};

export const getInvestmentPortfoliosOK = (portfolios) => {
  return {
    type: GET_INVESTMENT_PORTFOLIOS_OK,
    payload: portfolios,
  };
};

export const addNewInvestmentPortfolio = (newPortfolio) => {
  return {
    type: ADD_NEW_INVESTMENT_PORTFOLIO,
    payload: newPortfolio,
  };
};

export const resetAddNewInvestmentPortfolio = () => {
  return {
    type: RESET_ADD_NEW_INVESTMENT_PORTFOLIO,
  };
};

export const addNewInvestmentPortfolioOK = () => {
  return {
    type: ADD_NEW_INVESTMENT_PORTFOLIO_OK,
  };
};

export const editInvestmentPortfolio = (updatedPortfolio) => {
  return {
    type: EDIT_INVESTMENT_PORTFOLIO,
    payload: updatedPortfolio,
  };
};

export const editInvestmentPortfolioOK = () => {
  return {
    type: EDIT_INVESTMENT_PORTFOLIO_OK,
  };
};
export const resetEditInvestmentPortfolio = () => {
  return {
    type: RESET_EDIT_INVESTMENT_PORTFOLIO,
  };
};

export const deleteInvestmentPortfolio = (portfolioId) => {
  return {
    type: DELETE_INVESTMENT_PORTFOLIO,
    payload: portfolioId
  }
}
export const deleteInvestmentPortfolioOk = () => {
  return {
    type: DELETE_INVESTMENT_PORTFOLIO_OK
  }
}

export const getAllInvestmentPortfoliosAfterDeletion = (deletedPortfolioId, allPortfolios) => {
  return {
    type: GET_INVESTMENT_PORTFOLIOS_AFTER_DELETION,
    deletedPortfolioId,
    allPortfolios
  }
}

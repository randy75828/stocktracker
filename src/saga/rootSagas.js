import { takeEvery, call, put, fork, all } from "@redux-saga/core/effects";
import { InvestmentSaga } from "./InvestmentSaga";
export function* rootSagas() {
  yield all([fork(InvestmentSaga)]);
}
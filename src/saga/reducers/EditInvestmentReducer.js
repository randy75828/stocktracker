import {
  EDIT_INVESTMENT_PORTFOLIO,
  EDIT_INVESTMENT_PORTFOLIO_OK,
  RESET_EDIT_INVESTMENT_PORTFOLIO,
} from "../actions/InvestmentActionTypes";

const initialState = {
  loading: false,
  error: "",
  successfullyUpdated: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case EDIT_INVESTMENT_PORTFOLIO: {
      return { ...state, loading: true, successfullyUpdated: false };
    }

    case EDIT_INVESTMENT_PORTFOLIO_OK: {
      return {
        ...state,
        loading: false,
        successfullyUpdated: true,
      };
    }

    case RESET_EDIT_INVESTMENT_PORTFOLIO: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}

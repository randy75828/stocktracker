import {
    DELETE_INVESTMENT_PORTFOLIO,
    DELETE_INVESTMENT_PORTFOLIO_OK,
    RESET_DELETE_INVESTMENT_PORTFOLIO
} from '../actions/InvestmentActionTypes';


const initialState = {
    loading: false,
    error: "",
    successfullyDeleted: false
}

export default function(state = initialState, action){
    switch (action.type) {
        case DELETE_INVESTMENT_PORTFOLIO: {
            return { ...state, loading: true, successfullyDeleted: false }
        }

        case DELETE_INVESTMENT_PORTFOLIO_OK: {
            return {
                ...state,
                loading: false,
                successfullyDeleted: true,
            }
        }
        case RESET_DELETE_INVESTMENT_PORTFOLIO: {
            return initialState;
        }
        default: {
            return state;
        }
    }
}
import {
  ADD_NEW_INVESTMENT_PORTFOLIO,
  ADD_NEW_INVESTMENT_PORTFOLIO_OK,
  RESET_ADD_NEW_INVESTMENT_PORTFOLIO,
} from "../actions/InvestmentActionTypes";


const initialState = {
  loading: false,
  error: "",
  successfullyAdded: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_NEW_INVESTMENT_PORTFOLIO: {
      return { ...state, loading: true, successfullyAdded: false };
    }

    case ADD_NEW_INVESTMENT_PORTFOLIO_OK: {
      return {
        ...state,
        loading: false,
        successfullyAdded: true,
      };
    }
    case RESET_ADD_NEW_INVESTMENT_PORTFOLIO: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}

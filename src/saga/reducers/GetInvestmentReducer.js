import { GET_INVESTMENT_PORTFOLIOS, GET_INVESTMENT_PORTFOLIOS_OK, GET_INVESTMENT_PORTFOLIOS_AFTER_DELETION } from "../actions/InvestmentActionTypes";
const initialState = {
  loading: false,
  error: "",
  data: {
    portfolios: []
  }
};
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_INVESTMENT_PORTFOLIOS:
      {
        return {
          ...state,
          loading: true
        };
      }

    case GET_INVESTMENT_PORTFOLIOS_OK:
      {
        return {
          ...state,
          loading: false,
          error: "",
          data: {
            portfolios: action.payload
          }
        };
      }

    case GET_INVESTMENT_PORTFOLIOS_AFTER_DELETION:
      {
        return {
          ...state,
          loading: false,
          error: "",
          data: {
            portfolios: action.allPortfolios.filter(portfolio => portfolio._id !== action.deletedPortfolioId)
          }
        };
      }

    default:
      {
        return state;
      }
  }
}
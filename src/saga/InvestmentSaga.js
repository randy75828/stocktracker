import {
  ADD_NEW_INVESTMENT_PORTFOLIO,
  GET_INVESTMENT_PORTFOLIOS,
  EDIT_INVESTMENT_PORTFOLIO,
  EDIT_INVESTMENT_PORTFOLIO_OK,
  DELETE_INVESTMENT_PORTFOLIO,
  DELETE_INVESTMENT_PORTFOLIO_OK,
} from "./actions/InvestmentActionTypes";
import { takeEvery, call, put, fork, all } from "@redux-saga/core/effects";
import API from "./api/api";
import {
  getInvestmentPortfoliosOK,
  addNewInvestmentPortfolioOK,
  editInvestmentPortfolioOK,
  deleteInvestmentPortfolioOk,
} from "./actions/InvestmentActions";

//watcher
function* getAllInvestmentPortfolios() {
  yield takeEvery(GET_INVESTMENT_PORTFOLIOS, getAllInvestmentPortfoliosData);
}

function* addInvestmentPortfolio() {
  yield takeEvery(ADD_NEW_INVESTMENT_PORTFOLIO, addInvestmentPortfolioData);
}

function* updateInvestmentPortfolio() {
  yield takeEvery(EDIT_INVESTMENT_PORTFOLIO, updateInvestmentPortfolioData);
}

function* deleteInvestmentPortfolio() {
  yield takeEvery(DELETE_INVESTMENT_PORTFOLIO, deleteInvestmentPortfolioData);
}
//worker
function* getAllInvestmentPortfoliosData() {
  const investmentPorfoliosData = (yield getAllPortfolioData()).data;
  yield put(getInvestmentPortfoliosOK(investmentPorfoliosData));
}

function* addInvestmentPortfolioData(action) {
  const response = yield call(submitNewInvestmentPortfolioData, action.payload);
  if (response.status === 200) yield put(addNewInvestmentPortfolioOK());
}

function* updateInvestmentPortfolioData(action) {
  const response = yield call(
    submitUpdateInvestmentPortfolioData,
    action.payload
  );
  if (response.status === 200) yield put(editInvestmentPortfolioOK());
}

function* deleteInvestmentPortfolioData(action){
  const response = yield call(submitDeleteInvestmentPortfolioData,action.payload)
  console.log(response);
  if(response.status  ===200) yield put(deleteInvestmentPortfolioOk())
}

async function getAllPortfolioData() {
  return await API.get("/investments");
}

async function submitNewInvestmentPortfolioData(payload) {
  return await API.post("/investments/new", payload);
}

async function submitUpdateInvestmentPortfolioData(payload) {
  return await API.post("/investments/update",payload);
}

async function submitDeleteInvestmentPortfolioData(payload){
  return await API.delete("/investments/delete",{params: {portfolioId: payload}})
}

//Export all
export function* InvestmentSaga() {
  yield all([fork(getAllInvestmentPortfolios)]);
  yield all([fork(addInvestmentPortfolio)]);
  yield all([fork(updateInvestmentPortfolio)]);
  yield all([fork(deleteInvestmentPortfolio)]);
}

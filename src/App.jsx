import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import TradingTable from "./trade/TradingTable.jsx";
import HeaderTable from "./trade/TableHeader.jsx";
import Home from "./home/Home.jsx";
import Navbar from "./home/Navbar";
import InvestmentHomeContainer from "./investment/home/container/InvestmentHomeContainer";
import InvestmentPortfolio from "./investment/portfolio/InvestmentPortfolio";
import InvestmentPortfolioSummary from "./investment/summary/InvestmentPortfolioSummary";
import Loader from "./Loader/Loader";
import AddPortfolio from "./investment/portfolio/new/components/AddPortfolio";

//Redux
import { Provider } from "react-redux";
import store from "./saga/store.js";
import AddPortfolioContainer from "./investment/portfolio/new/container/AddPortfolioContainer";
import EditPortfolioContainer from "./investment/portfolio/edit/container/EditPortfolioContainer";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/trades">
            <HeaderTable />
            <TradingTable />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/investments">
            <InvestmentHomeContainer />
          </Route>
          <Route exact path="/investments/summary">
            <InvestmentPortfolioSummary />
          </Route>
          <Route exact path="/investments/randy">
            <InvestmentPortfolio />
          </Route>
          <Route exact path="/investments/new">
            <AddPortfolioContainer />
          </Route>
          <Route exact path="/investments/edit">
            <EditPortfolioContainer />
          </Route>
          <Route exact path="/investments/loader">
            <Loader />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

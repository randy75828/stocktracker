import {React} from 'react';


const Navbar =  () => {
return (
    <nav className="navbar navbar-expand-lg navbar-dark">
  <div className="container-fluid d-flex justify-content-around">
    <div>
    <a className="navbar-brand" href="#">Home</a>
    </div>
    <div>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    
    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div className="navbar-nav">
      
        <a className="nav-link" aria-current="page" href="/investments">Investments</a>
        <a className="nav-link" aria-current="page" href="/trades">Trades</a>
        <a className="nav-link active" aria-current="page" href="/login">Login</a>
        <a className="nav-link active" aria-current="page" href="/signup">Signup</a>
 
      </div>
    </div>
    </div>
  </div>
</nav>
)


}
export default Navbar;
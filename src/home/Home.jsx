import React from "react";
import "./Home.css";
import Navbar from "./Navbar";

const Home = () => {
  return (
    <div className="d-flex flex-column vh-100 home-background">
      <div>
        <Navbar />
      </div>
      <div className="home-header my-auto">
        <div className="d-flex flex-column align-items-center">
          <h1 className="display-1 text-white">StockTracker</h1>
          <hr className="home-header-border w-50" />
          <h5 className="text-white mb-3">
            One stop platform to track your investment/trades!
          </h5>
         <a href="/investments" className="btn btn-light">Let's go!</a>
        </div>
      </div>

      <div>
        <footer className="text-center mb-3 text-white">&copy; Randy 2021</footer>
      </div>
    </div>
  );
};

export default Home;

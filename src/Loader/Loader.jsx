import React from "react";
import LoaderIcon from "../images/LoaderIcon";

import "./Loader.css";

const Loader = () => {
  return (
    <div className="d-flex flex-column justify-content-center  align-items-center load">
      <div className="mb-3">
        <LoaderIcon width="5rem" height="5rem" fill="red" />
      </div>
      <div>
        <p className="lead">Loading in progress...</p>
      </div>
    </div>
  );
};

export default Loader;

import React from "react";
import {InvestmentReturnsIcon,InvestmentDailyProfitLossIcon,InvestmentDateOfInceptionIcon,InvestmentNetLiquidationIcon,InvestmentPortfolioTypeIcon } from "../../images";
import SummaryIndicator from "./SummaryIndicator";


const InvestmentPortfolioSummaryIndicators = () => {
  return (
    <div className=" col-lg-6">
      <h3 className="display-5">Key Indicators</h3>
      <div className="row">
        <SummaryIndicator
          title="Total Return"
          icon={<InvestmentReturnsIcon width="1.5em" height="1.5em" className="mb-2"/>}
          data="+56%"
          footer="Includes both realised and unrealised returns"
        />
        <SummaryIndicator
          title="Current Portfolio Return"
          icon={<InvestmentReturnsIcon width="1.5em" height="1.5em" className="mb-2"/>}
          data="+45%"
          footer="Gain/loss from your current portfolio (unrealised returns only)"
        />
        <SummaryIndicator
          title="Daily P/L"
          icon={<InvestmentDailyProfitLossIcon width="1.5em" height="1.5em" className="mb-2"/>}
          data="-0.5%"
          footer="Total gain/loss from your current portfolio today"
        />
        <SummaryIndicator
          title="Net Liquidation Value"
          icon={<InvestmentNetLiquidationIcon width="1.5em" height="1.5em" className="mb-2"/>}
          data="USD 12,452.23"
          footer="Total Liquidation value which includes your realised profits"
        />
      </div>
      <footer className="text-muted fst-italic mt-2">
        Accurate as of 18 June 2021
      </footer>
    </div>
  );
};

export default InvestmentPortfolioSummaryIndicators;

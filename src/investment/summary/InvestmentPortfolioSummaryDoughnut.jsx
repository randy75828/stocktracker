import React, { useEffect, useRef, useState } from "react";
import Chart from "chart.js/auto";
import "./InvestmentPortfolioSummaryDoughnut.css";

const InvestmentPortfolioSummaryDoughnut = () => {
  const data = {
    labels: ["AAPL", "GOOGL", "TSLA"],
    datasets: [
      {
        label: "My First Dataset",
        data: [25, 25, 50],
        backgroundColor: [
          "rgb(255, 99, 132)",
          "rgb(54, 162, 235)",
          "rgb(255, 205, 86)",
        ],
        hoverOffset: 4,
      },
    ],
  };

  const config = {
    type: "doughnut",
    data: data,
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        tooltip: {
          callbacks: {
            label: function (context) {
              return `${context.label}: ${context.raw}%`;
            },
          },
        },
      },
    },
  };

  const chartContainer = useRef(null);

  useEffect(() => {
    console.log(chartContainer);

    const newChartInstance = new Chart(chartContainer.current, config);
  }, []);

  return (
    <div className="col-lg-6 summary-doughnut mt-5 mt-lg-0">
      <h4 className="display-5 text-center">Portfolio Breakdown</h4>
      <canvas ref={chartContainer} className="my-5" />
    </div>
  );
};

export default InvestmentPortfolioSummaryDoughnut;

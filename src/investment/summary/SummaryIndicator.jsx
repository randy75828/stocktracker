import React from "react";
import getInvestmentIcon from "../../utils/investment/Constants";
import "./SummaryIndicator.css"

const SummaryIndicator = ({ title, icon, data, footer }) => {
 
  return (
    <div className="col-6 mt-3">
      <div className="card shadow">
        <div className="card-body d-flex flex-column align-items-center justify-content-between">
          <h5 className="card-title fs-5 text-center">
           <div>{icon}</div>
            {title}
          </h5>

          <p className="card-text fs-5">{data}</p>
          <footer className="card-subtitle mb-2 text-muted fw-light text-center">
            *{footer}
          </footer>
        </div>
      </div>
    </div>
  );
};
export default SummaryIndicator;

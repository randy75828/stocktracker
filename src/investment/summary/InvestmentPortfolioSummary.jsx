import React from "react";
import { useLocation,Link, useHistory } from "react-router-dom";
import InvestmentPortfolioSummaryDoughnut from "./InvestmentPortfolioSummaryDoughnut";
import InvestmentPortfolioSummaryIndicators from "./InvestmentPortfolioSummaryIndicators";
import { BackIcon } from "../../images";
const InvestmentPortfolioSummary =() => {
const location = useLocation()
const history = useHistory()
const portfolio = location.state;
  return (
    <>
    <div className="container">

    <div className="d-flex align-self-center mt-2">
        <div id="back" onClick={history.goBack}
        ><BackIcon width="1em" height="1em" /></div>
        <div className="ms-3 fs-5">Back to All Investment Portfolios</div>
      </div>
    <div className="row mt-5">
        <InvestmentPortfolioSummaryIndicators/>
        <InvestmentPortfolioSummaryDoughnut/>
      </div>
      <div className="d-flex justify-content-center">
        <Link to={{
          pathname:"/investments/edit",
          state: portfolio
        }} className="btn btn-success mt-3">Edit Portfolio</Link>
      </div>
    </div>
    </>
  );
};

export default InvestmentPortfolioSummary;

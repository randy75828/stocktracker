import React, { useState } from "react";
import {nanoid} from 'nanoid';

import PortfolioTable from "./PortfolioTable";

const InvestmentPortfolio = ({ }) => {
  const[newStocks, setNewStocks]  = useState([]);
  const addNewRow = () => {
    const newStock = {
      nanoid: nanoid(),
      tickerSymbol: "",
      name: "",
      sector: "",
      category: "",
      type: "",
      country: "",
      desiredAllocation: "",
      currentQuantity: "",
      averageCost: "",
      currentAllocation: "",
      intrinsicValue: "",
      discountPremium: "",
      portfolioRatio: "",
      totalProfitLoss: "",
    };
    newStocks.push(newStock);
    setNewStocks([...newStocks])
  };
  const portfolioStocks = [];
  const stocks = portfolioStocks.concat(newStocks);
  return (
    <div className="container">
      <div className="text-center display-4 mb-3">Your portfolio</div>
      <div className="d-flex justify-content-center">

          <PortfolioTable stocks={stocks} />
    
      </div>
      <button className="btn btn-primary mt-3" onClick={() => addNewRow()}>
        Add Row
      </button>
    </div>
  );
};

export default InvestmentPortfolio;

import { React, useState } from "react";
import PortfolioTable from "../../PortfolioTable";
import "./AddPortfolio.css";
import BackIcon from '../../../../images/BackIcon';

const AddPortfolio = ({
  addNewStock,
  deleteStock,
  submitForm,
  updateStockList,
  newPortfolio,
  updatePortfolioName,
  updatePortfolioType,
  updatePortfolioDateOfInception,
  goBack,
}) => {
  return (
 <div className="container">

      <div className="d-flex align-self-center mt-2">
        <div id="back" onClick={goBack}
        ><BackIcon width="1em" height="1em" /></div>
        <div className="ms-3 fs-5">Back to All Investment Portfolios</div>
      </div>
  

      <div className="text-center display-4 my-3"> Add Portfolio</div>
      <div className="row">
        <div className="col col-md-9 col-lg-6">
          <div className="form-group mb-3">
            <label htmlFor="portfolioName" className="mb-1">
              Portfolio Name
            </label>
            <input
              type="text"
              className="form-control"
              id="portfolioName"
              name="portfolioName"
              aria-describedby="emailHelp"
              onBlur={(e) => updatePortfolioName(e.target.value)}
            />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col col-md-9 col-lg-6">
          <div className="form-group mb-3">
            <label htmlFor="portfolioType" className="mb-1">
              Portfolio Type
            </label>
            <input
              type="text"
              className="form-control"
              id="portfolioType"
              name="portfolioType"
              onBlur={(e) => updatePortfolioType(e.target.value)}
            />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col col-md-9 col-lg-6">
          <div className="form-group mb-3">
            <label htmlFor="portfolioDateOfInception" className="mb-1">
              Date of inception
            </label>
            <input
              type="text"
              className="form-control"
              id="portfolioDateOfInception"
              name="portfolioDateOfInception"
              onBlur={(e) => updatePortfolioDateOfInception(e.target.value)}
            />
          </div>
        </div>
      </div>
      <div>
        <div className="mb-2">Portfolio Stock Components</div>
        <div className="d-flex justify-content-center">
          <PortfolioTable
            stocks={newPortfolio.components}
            updateStockList={updateStockList}
          />
        </div>
        <div className="row">
          <div className="col-auto">
            <div className="btn btn-primary mt-3 mb-3" onClick={addNewStock}>
              Add Row
            </div>
          </div>
          <div className="col-auto">
            <div className="btn btn-danger mt-3" onClick={deleteStock}>
              Delete Row
            </div>
          </div>
        </div>
      </div>

      <div>
        <button type="submit" className="btn btn-success" onClick={submitForm}>
          Submit
        </button>
        <div>
        </div>
      </div>
    </div>
  );
};

export default AddPortfolio;

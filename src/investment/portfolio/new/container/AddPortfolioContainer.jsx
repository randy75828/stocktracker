import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AddPortfolio from "../components/AddPortfolio";
import { addNewInvestmentPortfolio, resetAddNewInvestmentPortfolio } from "../../../../saga/actions/InvestmentActions";
import { nanoid } from "nanoid";
import { useHistory} from "react-router-dom";

const AddPortfolioContainer = () => {
  const createNewPorfolio = () => ({
    portfolioName: "",
    portfolioType: "",
    dateOfInception: "",
    components: [createNewStock()],
  });

  const createNewStock = () => ({
    nanoid: nanoid(),
    tickerSymbol: "",
    name: "",
    sector: "",
    category: "",
    type: "",
    country: "",
    currentQuantity: "",
    currentAllocation: "",
    // averageCost: "",
    intrinsicValue: "",
    desiredAllocation: "", // discountPremium: "",
    // portfolioRatio: "",
    // totalProfitLoss: "",
  });

  const newPortfolioSuccessfullyAdded = useSelector((state) => {
    console.log(state.AddInvestmentReducer);
    return state.AddInvestmentReducer.successfullyAdded;
  });
  useEffect(() => {
    if (newPortfolioSuccessfullyAdded) {
      history.push("/investments");
    }
    return dispatch(resetAddNewInvestmentPortfolio())
  }, [newPortfolioSuccessfullyAdded]);
  const history = useHistory();
  const dispatch = useDispatch();
  const [newPortfolio, setNewPortfolio] = useState(createNewPorfolio());

  const addNewStock = () => {
    setNewPortfolio((portfolio) => {
      return {
        ...portfolio,
        components: [...portfolio.components, createNewStock()],
      };
    });
  };

  const deleteStock = () => {
    setNewPortfolio((portfolio) => {
      const updatedPortfolioComponents = [...portfolio.components];
      updatedPortfolioComponents.pop();
      console.log(portfolio);
      return { ...portfolio, components: updatedPortfolioComponents };
    });
  };

  const updateStockList = (updatedStockNanoId, updatedStock) => {
    const updatedStocks = newPortfolio.components.map((stock) =>
      stock.nanoid === updatedStockNanoId ? { ...updatedStock } : stock
    );
    setNewPortfolio((portfolio) => {
      return { ...portfolio, components: updatedStocks };
    });
  };

  const updatePortfolioName = (portfolioName) => {
    setNewPortfolio({ ...newPortfolio, portfolioName });
  };

  const updatePortfolioType = (portfolioType) => {
    setNewPortfolio({ ...newPortfolio, portfolioType });
  };

  const updatePortfolioDateOfInception = (dateOfInception) => {
    setNewPortfolio({ ...newPortfolio, dateOfInception });
  };

  const goBack = () => {
    history.goBack();
  };

  const submitForm = () => {
    const portfolioComponents = newPortfolio.components.map((stock) => {
      const { nanoid, ...stockData } = stock;
      return stockData;
    });
    const newPortfolioToBeSaved = {
      ...newPortfolio,
      components: portfolioComponents,
    };
    dispatch(addNewInvestmentPortfolio(newPortfolioToBeSaved));
  };

  return (
    <AddPortfolio
      submitForm={submitForm}
      addNewStock={addNewStock}
      deleteStock={deleteStock}
      updateStockList={updateStockList}
      newPortfolio={newPortfolio}
      updatePortfolioName={updatePortfolioName}
      updatePortfolioType={updatePortfolioType}
      updatePortfolioDateOfInception={updatePortfolioDateOfInception}
      goBack={goBack}
    />
  );
};

export default AddPortfolioContainer;

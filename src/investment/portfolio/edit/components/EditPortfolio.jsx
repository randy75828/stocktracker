import { React, useState } from "react";
import BackIcon from '../../../../images/BackIcon';
import PortfolioTable from "../../PortfolioTable";


const EditPortfolio = ({
    addNewStock,
    deleteStock,
    updateForm,
    updateStockList,
    currentPortfolio,
    updatePortfolioName,
    updatePortfolioType,
    updatePortfolioDateOfInception,
    goBack,
  }) => {
    console.log(currentPortfolio)
    return (
        <div className="container">
       
             <div className="d-flex align-self-center mt-2">
               <div id="back" onClick={goBack}
               ><BackIcon width="1em" height="1em" /></div>
               <div className="ms-3 fs-5">Back to Summary Page</div>
             </div>
         
       
             <div className="text-center display-4 my-3">Edit Portfolio</div>
             <div className="row">
               <div className="col col-md-9 col-lg-6">
                 <div className="form-group mb-3">
                   <label htmlFor="portfolioName" className="mb-1">
                     Portfolio Name
                   </label>
                   <input
                     type="text"
                     className="form-control"
                     id="portfolioName"
                     name="portfolioName"
                     aria-describedby="emailHelp"
                     defaultValue={currentPortfolio.portfolioName}
                     onBlur={(e) => updatePortfolioName(e.target.value)}
                   />
                 </div>
               </div>
             </div>
             <div className="row">
               <div className="col col-md-9 col-lg-6">
                 <div className="form-group mb-3">
                   <label htmlFor="portfolioType" className="mb-1">
                     Portfolio Type
                   </label>
                   <input
                     type="text"
                     className="form-control"
                     id="portfolioType"
                     name="portfolioType"
                     defaultValue={currentPortfolio.portfolioType}
                     onBlur={(e) => updatePortfolioType(e.target.value)}
                   />
                 </div>
               </div>
             </div>
             <div className="row">
               <div className="col col-md-9 col-lg-6">
                 <div className="form-group mb-3">
                   <label htmlFor="portfolioDateOfInception" className="mb-1">
                     Date of inception
                   </label>
                   <input
                     type="text"
                     className="form-control"
                     id="portfolioDateOfInception"
                     name="portfolioDateOfInception"
                     defaultValue={currentPortfolio.dateOfInception}
                     onBlur={(e) => updatePortfolioDateOfInception(e.target.value)}
                   />
                 </div>
               </div>
             </div>
             <div>
               <div className="mb-2">Portfolio Stock Components</div>
               <div className="d-flex justify-content-center">
                 <PortfolioTable
                   stocks={currentPortfolio.components}
                   updateStockList={updateStockList}
                 />
               </div>
               <div className="row">
                 <div className="col-auto">
                   <div className="btn btn-primary mt-3 mb-3" onClick={addNewStock}>
                     Add Row
                   </div>
                 </div>
                 <div className="col-auto">
                   <div className="btn btn-danger mt-3" onClick={deleteStock}>
                     Delete Row
                   </div>
                 </div>
               </div>
             </div>
       
             <div>
               <button type="submit" className="btn btn-success" onClick={updateForm}>
                 Save changes
               </button>
               <div>
               </div>
             </div>
           </div>
         );
}

export default EditPortfolio;
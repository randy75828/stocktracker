import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  editInvestmentPortfolio, resetEditInvestmentPortfolio,
} from "../../../../saga/actions/InvestmentActions";
import { nanoid } from "nanoid";
import { useHistory, useLocation } from "react-router-dom";
import EditPortfolio from "../../edit/components/EditPortfolio";

const EditPortfolioContainer = () => {
  const createNewStock = () => ({
    nanoid: nanoid(),
    tickerSymbol: "",
    name: "",
    sector: "",
    category: "",
    type: "",
    country: "",
    currentQuantity: "",
    currentAllocation: "",
    // averageCost: "",
    intrinsicValue: "",
    desiredAllocation: "", // discountPremium: "",
    // portfolioRatio: "",
    // totalProfitLoss: "",
  });

  const location = useLocation();
  const currentComponentsWithNanoId = location.state.components.map(
    (stock) => ({ ...stock, nanoid: stock._id })
  );

  const currentPortfolioData = {
    ...location.state,
    components: currentComponentsWithNanoId,
  };

  const portfolioSuccessfullyUpdated = useSelector((state) => {
    console.log(state.EditInvestmentReducer);
    return state.EditInvestmentReducer.successfullyUpdated;
  });

  useEffect(() => {
    if (portfolioSuccessfullyUpdated) {
      history.push("/investments");
    }
    return dispatch(resetEditInvestmentPortfolio())
  }, [portfolioSuccessfullyUpdated]);

  const history = useHistory();
  const dispatch = useDispatch();
  const [currentPortfolio, setCurrentPortfolio] = useState(currentPortfolioData);

  const addNewStock = () => {
    setCurrentPortfolio((currentPortfolio) => {
      return {
        ...currentPortfolio,
        components: [...currentPortfolio.components, createNewStock()],
      };
    });
  };

  const deleteStock = () => {
    setCurrentPortfolio((currentPortfolio) => {
      const updatedPortfolioComponents = [...currentPortfolio.components];
      updatedPortfolioComponents.pop();
      console.log(currentPortfolio);
      return { ...currentPortfolio, components: updatedPortfolioComponents };
    });
  };

  const updateStockList = (updatedStockNanoId, updatedStock) => {
    const updatedStocks = currentPortfolio.components.map((stock) =>
      stock.nanoid === updatedStockNanoId ? { ...updatedStock } : stock
    );
    setCurrentPortfolio((currentPortfolio) => {
      return { ...currentPortfolio, components: updatedStocks };
    });
  };

  const updatePortfolioName = (portfolioName) => {
    setCurrentPortfolio({ ...currentPortfolio, portfolioName });
  };

  const updatePortfolioType = (portfolioType) => {
    setCurrentPortfolio({ ...currentPortfolio, portfolioType });
  };

  const updatePortfolioDateOfInception = (dateOfInception) => {
    setCurrentPortfolio({ ...currentPortfolio, dateOfInception });
  };

  const goBack = () => {
    history.goBack();
  };

  const updateForm = () => {
    const portfolioComponents = currentPortfolio.components.map((stock) => {
      const { nanoid, ...stockData } = stock;
      return stockData;
    });
    const updatedPortfolioToBeSaved = {
      ...currentPortfolio,
      components: portfolioComponents,
    };
    dispatch(editInvestmentPortfolio(updatedPortfolioToBeSaved));
  };

  return (
    <EditPortfolio
      updateForm={updateForm}
      addNewStock={addNewStock}
      deleteStock={deleteStock}
      updateStockList={updateStockList}
      updatePortfolioName={updatePortfolioName}
      updatePortfolioType={updatePortfolioType}
      updatePortfolioDateOfInception={updatePortfolioDateOfInception}
      goBack={goBack}
      currentPortfolio={currentPortfolio}
    />
  );
};

export default EditPortfolioContainer;

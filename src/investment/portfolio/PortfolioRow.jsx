import React from "react";
import "./PortfolioRow.css";

const PortfolioRow = ({ updateStockList, stock }) => {
  const updatePortfolioComponentString = (e, updatedStockNanoId) => {
    const updatedField = e.target.getAttribute("name");
    const updatedValue = e.target.value;
    const updatedStock = {
      ...stock,
      [updatedField]: updatedValue,
    };
    updateStockList(updatedStockNanoId, updatedStock);
  };

  const updatePortfolioComponentNumber = (e, updatedStockNanoId) => {
    const updatedField = e.target.getAttribute("name");
    const updatedValue = e.target.value;
    const updatedStock = {
      ...stock,
      [updatedField]: parseFloat(updatedValue),
    };
    updateStockList(updatedStockNanoId, updatedStock);
  };
  return (
    <>
      <div className="portfolio-table-row">
        <div className="portfolio-table-cell border border-1">
          <input
            name="tickerSymbol"
            defaultValue={stock.tickerSymbol}
            className="form-control"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.name}
            className="form-control"
            name="name"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.sector}
            className="form-control"
            name="sector"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.category}
            className="form-control"
            name="category"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.type}
            className="form-control"
            name="type"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.country}
            className="form-control"
            name="country"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.currentQuantity}
            className="form-control"
            name="currentQuantity"
            onBlur={(e) => updatePortfolioComponentNumber(e, stock.nanoid)}
          />
        </div>

        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.currentAllocation}
            className="form-control"
            name="currentAllocation"
            onChange={(e) => updatePortfolioComponentNumber(e, stock.nanoid)}
          />
        </div>
        {/* <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.averageCost}
            className="form-control"
            name="averageCost"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div> */}
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.intrinsicValue}
            className="form-control"
            name="intrinsicValue"
            onChange={(e) => updatePortfolioComponentNumber(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.desiredAllocation}
            className="form-control"
            name="desiredAllocation"
            onChange={(e) => updatePortfolioComponentNumber(e, stock.nanoid)}
          />
        </div>
        {/* <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.discountPremium}
            className="form-control"
            name="discountPremium"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.portfolioRatio}
            className="form-control"
            name="portfolioRatio"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div>
        <div className="portfolio-table-cell border border-1">
          <input
            type="text"
            defaultValue={stock.totalProfitLoss}
            className="form-control"
            name="totalProfitLoss"
            onBlur={(e) => updatePortfolioComponentString(e, stock.nanoid)}
          />
        </div> */}
      </div>
    </>
  );
};
export default PortfolioRow;

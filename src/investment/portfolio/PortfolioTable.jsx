import React from "react";
import "./PortfolioTable.css";
import PortfolioRow from "./PortfolioRow";

const PortfolioTable = ({ stocks, updateStockList }) => {
  return (
    <div className="table table-responsive">
      <div className="portfolio-table">
        <div className="portfolio-table-cell-header">
          <div className="portfolio-table-header-cell border border-1">
            Ticker Symbol
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Name
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Sector
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Category
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Type
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Country
          </div>
          {/* <div className="portfolio-table-header-cell border border-1 ">
            Average Cost
          </div> */}
          <div className="portfolio-table-header-cell border border-1 ">
            Current Quantity
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Current Allocation (USD)
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Intrinsic Value (USD)
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Desired Allocation (USD)
          </div>
          {/* <div className="portfolio-table-header-cell border border-1 ">
            Discount/Premium
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Portfolio Ratio
          </div>
          <div className="portfolio-table-header-cell border border-1 ">
            Total P/L
          </div> */}
        </div>
        <div className="portfolio-table-body">
          {stocks.map((stock, index) => {
            return (
              <PortfolioRow
                stock={stock}
                key={stock.nanoid}
                updateStockList={updateStockList}
                index={index}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default PortfolioTable;

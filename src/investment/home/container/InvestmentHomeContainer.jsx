import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../../Loader/Loader";
import InvestmentHome from "../components/InvestmentHome";
import { getInvestmentPortfolios, deleteInvestmentPortfolio, getAllInvestmentPortfoliosAfterDeletion } from "../../../saga/actions/InvestmentActions";

const InvestmentHomeContainer = () => {

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getInvestmentPortfolios());
  }, []);

  const investmentPortfolioState = useSelector((state) => {
    return state.GetInvestmentReducer;
  });
  const allPortfolios = investmentPortfolioState.data.portfolios;

  console.log(allPortfolios);
  const loading = investmentPortfolioState.loading




  const deletePortfolio = (portfolioId) => {
    console.log(portfolioId);
    dispatch(deleteInvestmentPortfolio(portfolioId))
    dispatch(getAllInvestmentPortfoliosAfterDeletion(portfolioId, allPortfolios))
  }


  return (
    <div className="container">
      {loading ? <Loader /> : <InvestmentHome allPortfolios={allPortfolios} deletePortfolio={deletePortfolio} />}
    </div>
  );
};

export default InvestmentHomeContainer;

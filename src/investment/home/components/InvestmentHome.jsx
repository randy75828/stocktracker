import React from "react";
import PortfolioListItem from "./PortfolioListItem";
import { Link } from "react-router-dom";

const InvestmentHome = ({ allPortfolios,deletePortfolio }) => {
  return (
    <div>
      <h1 className="display-2 text-center mb-5">
        Current Investment Portfolios
      </h1>
      <div>
        {allPortfolios.map((portfolio) => {
          return <PortfolioListItem portfolio={portfolio} key={portfolio._id} deletePortfolio = {deletePortfolio}/>;
        })}
      </div>
      <Link className="btn btn-dark text-white" to="/investments/new">
        Add New Portfolio
      </Link>
    </div>
  );
};

export default InvestmentHome;

import React, { useState } from "react";
import styled from "styled-components";
import {
  InvestmentReturnsIcon,
  InvestmentPortfolioTypeIcon,
  InvestmentDateOfInceptionIcon,
} from "../../../images";

import { Link } from "react-router-dom";
import DeletePortfolioModal from "./DeletePortfolioModal";
import "./PortfolioListItem.css"

const PortfolioListItem = ({ portfolio, deletePortfolio }) => {
  const {
    portfolioName,
    currentPortfolioReturns,
    portfolioType,
    dateOfInception,
    _id: portfolioId
  } = portfolio;

  const CurrentPortfolioReturns = styled.span`
    color: ${props => {
      if (props.state === "positive") return "green";
      if (props.state === "negative") return "red";
      if (props.state === "neutral") return "black";
    }}
  
  `;

  const getCurrentPortfolioReturnState = (returnAmount) => {
    if (returnAmount > 0)
      return "positive"
    if (returnAmount < 0)
      return "negative"
    if (returnAmount === 0)
      return "neutral"
    return "neutral"
  }

  const getFormattedCurrentPortfoliosAmount = (returnAmount) => {
    if (returnAmount === 0)
      return `${returnAmount}%`
    if (returnAmount > 0)
      return `+${returnAmount}%`
    if (returnAmount < 0)
      return `${returnAmount}%`
    return `${returnAmount}%`
  }


  return (
    <div className="row d-flex-column justify-content-center align-items-center">
      <div className="col-auto me-2">
        <DeletePortfolioModal portfolioId={portfolioId} deletePortfolio={deletePortfolio} />
      </div>
      <div className="card my-3 shadow col">
        <div className="card-body row">
          <div className="col-3 border-end d-flex align-items-center justify-content-center">
            <h5 className="card-title display-6 text-center">
              {portfolioName}
            </h5>
          </div>
          <div className="col-9  row d-flex align-items-center">
            <div className="col-9 d-flex flex-column justify-content-around align-self-center">
              <div>
                <p className="ms-2 fs-3">
                  <InvestmentReturnsIcon
                    width="2em"
                    height="2em"
                    className="me-2"
                  />
                  Current portfolio returns:
                  <CurrentPortfolioReturns className="fw-bold ms-1" state={getCurrentPortfolioReturnState(currentPortfolioReturns)}>{getFormattedCurrentPortfoliosAmount(currentPortfolioReturns)}</CurrentPortfolioReturns>
                </p>
              </div>

              <div>
                <p className="ms-2 fs-4">
                  <InvestmentPortfolioTypeIcon
                    className="me-2"
                    width="1.5em"
                    height="1.5em"
                  />
                  Portfolio type:
                  <span className="ms-1">{portfolioType}</span>
                </p>
              </div>

              <div>
                <p className="ms-2 fs-5">
                  <InvestmentDateOfInceptionIcon
                    className="me-2"
                    width="1.5em"
                    height="1.5em"
                  />
                  Date of inception:
                  <span className="ms-1">{dateOfInception}</span>
                </p>
              </div>
            </div>
            <div className="col-3">
              <Link
                to={{
                  pathname: "/investments/summary",
                  state: portfolio,
                }}
                className="btn btn-primary"
              >
                View Portfolio Summary
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
};

export default PortfolioListItem;

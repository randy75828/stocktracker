import React from 'react';
import './DeletePortfolioModal.css';
import styled from 'styled-components';
import { DeleteIcon } from '../../../images';



const DeletePortfolioModal = ({ portfolioId, deletePortfolio }) => {
    return (
        <div>
            <DeleteIcon width="2em" height="2em" className="clickable" data-bs-toggle="modal" data-bs-target="#exampleModal" />
            <div>
                <div className="modal fade" id="exampleModal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-body border border-1 rounded-3">
                                <div className="row">
                                    <div className="col text-center">
                                        Are you sure you want to delete this portfolio?
                                    </div>
                                </div>
                                <div className="row justify-content-around">
                                    <div className="col-4 mt-4 btn btn-primary" onClick={() => deletePortfolio(portfolioId)} data-bs-dismiss="modal">
                                        Yes
                                    </div>
                                    <div className="col-4 mt-4 btn btn-secondary" data-bs-dismiss="modal">
                                        No
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    )

}

export default DeletePortfolioModal;
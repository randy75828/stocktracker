import {InvestmentReturnsIcon,InvestmentDailyProfitLossIcon,InvestmentDateOfInceptionIcon,InvestmentNetLiquidationIcon,InvestmentPortfolioTypeIcon } from "../../images";


const getInvestmentIcon = (icon) => {
  if (icon === "CURRENT_PORTFOLIO_RETURNS" || icon === "TOTAL_RETURNS") return <InvestmentReturnsIcon/>
  if (icon === "DATE_OF_INCEPTION") return <InvestmentDateOfInceptionIcon/>
  if (icon === "DAILY_PL") return <InvestmentDailyProfitLossIcon/>;
  if (icon === "NET_LIQUIDATION") return <InvestmentNetLiquidationIcon/>;
  if (icon === "PORTFOLIO_TYPE") return <InvestmentPortfolioTypeIcon/>;
};

export default getInvestmentIcon;

import React, { useState } from "react";
import './TradingTable.css';

const TradingTable = () => {
  return <div className="container">
      <table className="trading-table table table-bordered">
        <thead>
          <tr>
            <th scope="col">Entry Date</th>
            <th scope="col">Direction</th>
            <th scope="col">Stock (Ticker Symbol)</th>
            <th scope="col">Entry</th>
            <th scope="col">Actual Entry Price</th>
            <th scope="col">Stop Loss</th>
            <th scope="col">1R</th>
            <th scope="col">2R</th>
            <th scope="col">Profit Target</th>
            <th scope="col">Actual Exit Price</th>
            <th scope="col">Exit Date</th>
            <th scope="col">R multiple</th>
            <th scope="col">Comments</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>23/03/2021</td>
            <td>Long</td>
            <td>Dip On Uptrend 50 MA</td>
            <td>$78.12</td>
            <td>$78</td>
            <td>$73.33</td>
            <td>$4.55</td>
            <td>$9.88</td>
            <td>$87.20</td>
             <td>$99.00</td>
             <td>25/03/2021</td>
             <td>2.56</td>
             <td>Trading is so easy!</td>
          </tr>
        </tbody>
      </table>
    </div>
};

export default TradingTable;
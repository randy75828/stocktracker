import React, { useState } from "react";
import './TableHeader.css';

const TableHeader = () => {
  return (
    <div className="table-header">
      <div className="container">
        <div className="month">
          <h2 className="display-4">September 2020</h2>
        </div>
        <div className="totalRisk">
          <h4>Total R (Risk): 5.25</h4>
        </div>
        <div className="totalTrades">
          <h4>No. of trades: 10</h4>
        </div>
        <div className="totalExpectancyTrade">
          <h4>Expectancy per trade: 2.3</h4>
        </div>
      </div>
    </div>
  );
};

export default TableHeader;


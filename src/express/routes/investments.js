const express = require('express');
const router = express.Router();

const investments = require('../controllers/investments');


router.get('/', investments.getAllInvestmentPortfolios);
router.post('/new', investments.createInvestmentPortfolio);
router.post('/update', investments.updateInvestmentPortfolio);
router.delete('/delete',investments.deleteInvestmentPortfolio);


module.exports = router;
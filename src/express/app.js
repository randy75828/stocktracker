const express = require('express');
const cors = require('cors');
const app  = express();
//To enable reading of form requests
//Not required because react is sending in the form of json.
app.use(express.urlencoded({ extended: true }));
//Enable reading of json body requests
app.use(express.json());
app.use(cors());
const port = process.env.PORT || 5000;


const mongoose  = require('mongoose');
mongoose.connect('mongodb://localhost:27017/randy-portfolio', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
    console.log("Database connected");
});

const investmentRoutes = require('./routes/investments');

app.listen(port, () => console.log(`Listening on PORT ${port}`));




app.use('/investments',investmentRoutes);

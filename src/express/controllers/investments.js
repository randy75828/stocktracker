const InvestmentPortfolio = require("../models/InvestmentPortfolio");
module.exports.getAllInvestmentPortfolios = async (req, res) => {
  console.log("GET ALL INVESTMENT PORTFOLIOS HIT!");

  const portfolios = await InvestmentPortfolio.find({});
  const response = portfolios.map((portfolio) => ({
    ...portfolio.toObject(),
    currentPortfolioReturns: 45,
  }));
  // const portfolios =
  //   {
  //     name: "Randyyy",
  //     nanoid: "1234",
  //     dateOfInception: "Sept 2019",
  //     portfolioType: "Technology Yo",
  //     currentPortfolioReturns: "+45%",
  //   },

  //   {
  //     name: "Raymond",
  //     nanoid: "5678",
  //     dateOfInception: "June 2019",
  //     portfolioType: "General",
  //     currentPortfolioReturns: "-25%",
  //   },
  // ];

  return res.json(response);
};

module.exports.createInvestmentPortfolio = async (req, res) => {
  console.log("ADD NEW PORTFOLIO HIT!");
  console.log(req.body);
  const investmentPortfolio = new InvestmentPortfolio(req.body);
  await investmentPortfolio.save();
  res.send("Successfully Saved!");
};

module.exports.updateInvestmentPortfolio = async (req, res) => {
  console.log("UPDATE PORTFOLIO HIT!");
  console.log(`UPDATING ${req.body.portfolioName} portfolio`);
  const investmentPortfolio = new InvestmentPortfolio(req.body);
  console.log(investmentPortfolio);
  await InvestmentPortfolio.findByIdAndUpdate(investmentPortfolio._id, investmentPortfolio, { runValidators: true });
  res.send("Successfully updated!");
};

module.exports.deleteInvestmentPortfolio = async (req, res) => {
  console.log("DELETE PORTFOLIO HIT!");
  const { portfolioId } = req.query
  await InvestmentPortfolio.findByIdAndDelete(portfolioId);
  res.send(`Successfully deleted portfolioId : ${portfolioId}!`);
}

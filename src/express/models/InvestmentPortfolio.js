const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const InvestmentPortfolioComponentsSchema = new Schema({
  tickerSymbol: { type: String, required: true },
  name: { type: String, required: true },
  sector: { type: String, required: true },
  category: { type: String, required: true },
  type: { type: String, required: true },
  country: { type: String, required: true },
  currentQuantity: {
    type: Number,
    required: true,
    min: [1, "CurrentQuantity should be positive"],
  },
  currentAllocation: { type: Number, required: true },
  intrinsicValue: { type: Number, required: true },
  desiredAllocation: { type: Number, required: true },
});

const InvestmentPortfolioSchema = new Schema({
    portfolioName: { type: String, required: true },
    portfolioType: { type: String, required: true },
  dateOfInception: { type: String, required: true },
  components: [InvestmentPortfolioComponentsSchema],
});

module.exports = mongoose.model(
  "InvestmentPortfolio",
  InvestmentPortfolioSchema
);
